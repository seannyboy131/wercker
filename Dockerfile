FROM ubuntu:latest
# Creating user
RUN groupadd -r ubuntu && useradd -r -g ubuntu ubuntu

COPY helloworld/helloworld.sh /home/ubuntu/helloworld.sh
RUN chmod +x /home/ubuntu/helloworld.sh
USER ubuntu
WORKDIR /home/ubuntu
